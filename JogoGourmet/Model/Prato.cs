﻿namespace JogoGourmet.Model
{
    public class Prato
    {
        public int Id { get; set; }
        public Prato PratoAssociado { get; set; }
        public string Nome { get; set; }
    }
}
