﻿using JogoGourmet.Enums;
using System;
using System.Windows;

namespace JogoGourmet.View
{
    /// <summary>
    /// Lógica interna para QuestionWindow.xaml
    /// </summary>
    public partial class QuestionWindow : Window
    {
        #region Properties
        public TipoViewEnum TipoView;

        public event EventHandler OkClick;
        public event EventHandler CancelarClick;

        public string Resposta { get { return tbResposta != null ? tbResposta.Text : string.Empty; } }
        #endregion

        #region Constructors
        public QuestionWindow()
        {
            InitializeComponent();
        }
        #endregion

        #region Methods
        public void CarregarQuestao()
        {
            Title = "Desisto";

            TipoView = TipoViewEnum.Desisto;
            tbQuestao.Text = "Qual prato você pensou?";

            ShowDialog();
        }

        public void CarregarQuestao(string nomePrato, string nomeNovoPrato)
        {
            Title = "Complete";

            TipoView = TipoViewEnum.Complete;
            tbQuestao.Text = $"{nomeNovoPrato} é ______ mas {nomePrato} não.";

            ShowDialog();
        }

        private void Ok_Click(object sender, RoutedEventArgs e)
        {
            if (OkClick != null)
                OkClick.Invoke(this, new EventArgs());

            EsconderComponente();
        }

        private void Cancelar_Click(object sender, RoutedEventArgs e)
        {
            if (CancelarClick != null)
                CancelarClick.Invoke(this, new EventArgs());

            EsconderComponente();
        }

        private void EsconderComponente()
        {
            tbResposta.Clear();
            this.Hide();
        }
        #endregion
    }
}
