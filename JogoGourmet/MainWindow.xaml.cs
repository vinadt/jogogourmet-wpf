﻿using JogoGourmet.Business;
using JogoGourmet.Business.Interfaces;
using JogoGourmet.View;
using System;
using System.Windows;

namespace JogoGourmet
{
    /// <summary>
    /// Interação lógica para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, IInteracao
    {
        #region Fields
        private GourmetBusiness _gourmetBusiness;
        private QuestionWindow _questionWindow;
        #endregion

        #region Constructors
        public MainWindow()
        {
            InitializeComponent();

            _questionWindow = new QuestionWindow();
            _questionWindow.OkClick += QuestionWindow_OkClick;
            _questionWindow.CancelarClick += QuestionWindow_CancelarClick;

            _gourmetBusiness = new GourmetBusiness(this);
        }
        #endregion

        #region Methods
        #region IInteracao implementations
        public void CarregarInterfaceComplete(string nomePrato, string nomeNovoPrato)
        {
            _questionWindow.CarregarQuestao(nomePrato, nomeNovoPrato);
        }

        public void CarregarInterfaceDesisto()
        {
            _questionWindow.CarregarQuestao();
        }

        public void EmitirMensagemSucesso()
        {
            MessageBox.Show("Acertei de novo!");
        }

        public bool QuestionarUsuario(string questao)
        {
            return MessageBox.Show(questao, "Confirm", MessageBoxButton.YesNo) == MessageBoxResult.Yes;
        }
        #endregion

        private void QuestionWindow_CancelarClick(object sender, EventArgs e)
        {
            _gourmetBusiness.LimparNovosPratos();
        }

        private void QuestionWindow_OkClick(object sender, EventArgs e)
        {
            switch (_questionWindow.TipoView)
            {
                case Enums.TipoViewEnum.Desisto:
                    var nomeNovoPrato = _questionWindow.Resposta;
                    _gourmetBusiness.CriarNovoPrato(nomeNovoPrato);
                    break;
                case Enums.TipoViewEnum.Complete:
                    var nomeNovoPratoAssociado = _questionWindow.Resposta;
                    _gourmetBusiness.CriarNovoPratoAssociado(nomeNovoPratoAssociado);
                    break;
                default:
                    break;
            }
        }

        private void Ok_Click(object sender, RoutedEventArgs e)
        {
            _gourmetBusiness.AdivinharPrato();
        }
        #endregion
    }
}
