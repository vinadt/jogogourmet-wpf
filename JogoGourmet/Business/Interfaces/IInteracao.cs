﻿namespace JogoGourmet.Business.Interfaces
{
    public interface IInteracao
    {
        void CarregarInterfaceComplete(string nomePrato, string nomeNovoPrato);
        void CarregarInterfaceDesisto();
        void EmitirMensagemSucesso();
        bool QuestionarUsuario(string questao);
    }
}
