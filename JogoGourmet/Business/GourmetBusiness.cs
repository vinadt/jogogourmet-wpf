﻿using JogoGourmet.Business.Interfaces;
using JogoGourmet.Model;
using System.Collections.Generic;
using System.Linq;

namespace JogoGourmet.Business
{
    public class GourmetBusiness
    {
        #region Fields
        private IInteracao _interacao;

        private List<Prato> _pratos;
        private Prato _novoPrato;
        private Prato _novoPratoAssociado;

        private string _questao { get { return "O prato que você pensou é {0}?"; } }
        #endregion

        #region Constructors
        public GourmetBusiness(IInteracao interfaceDeInteracao)
        {
            _interacao = interfaceDeInteracao;
            CarregarPratosIniciais();
        }
        #endregion

        #region Methods
        public void AdivinharPrato()
        {
            var pratosFiltrados = BuscarPratosDoPratoAssociado(string.Empty);
            ExecutarBuscaDePratos(pratosFiltrados);
        }

        public void CriarNovoPrato(string nomeNovoPrato)
        {
            if (!string.IsNullOrWhiteSpace(nomeNovoPrato))
                _novoPrato = new Prato { Nome = nomeNovoPrato };
            else
                _novoPrato = null;
        }

        public void CriarNovoPratoAssociado(string nomeNovoPratoAssociado)
        {
            if (!string.IsNullOrWhiteSpace(nomeNovoPratoAssociado))
            {
                _novoPratoAssociado = new Prato { Nome = nomeNovoPratoAssociado };
                _novoPrato.PratoAssociado = _novoPratoAssociado;
            }
            else
            {
                _novoPrato = null;
                _novoPratoAssociado = null;
            }
        }

        public void LimparNovosPratos()
        {
            _novoPratoAssociado = null;
            _novoPrato = null;
        }

        private void CarregarPratosIniciais()
        {
            _pratos = new List<Prato>();

            var massa = new Prato { Id = 10, Nome = "massa" };
            var lasanha = new Prato { Id = 10, Nome = "Lasanha", PratoAssociado = massa };
            var boloDeChocolate = new Prato { Id = 20, Nome = "Bolo de chocolate" };

            _pratos.Add(massa);
            _pratos.Add(lasanha);
            _pratos.Add(boloDeChocolate);
        }

        private List<Prato> BuscarPratosDoPratoAssociado(string pratoAssociado)
        {
            return _pratos.Where(p => string.IsNullOrWhiteSpace(pratoAssociado) ? p.PratoAssociado == null : p.PratoAssociado != null && p.PratoAssociado.Nome == pratoAssociado).OrderByDescending(o => o.PratoAssociado != null).ThenBy(t => t.Id).ToList();
        }

        private void ExecutarBuscaDePratos(List<Prato> pratos)
        {
            Prato pratoAtual = null;
            foreach (var prato in pratos)
            {
                if (_interacao.QuestionarUsuario(string.Format(_questao, prato.Nome)))
                {
                    var pratosFilhos = BuscarPratosDoPratoAssociado(prato.Nome);
                    if (pratosFilhos != null && pratosFilhos.Count > 0)
                    {
                        ExecutarBuscaDePratos(pratosFilhos);
                        return;
                    }
                    else
                    {
                        _interacao.EmitirMensagemSucesso();
                        return;
                    }
                }
                else
                    pratoAtual = prato;
            }

            IncluirNovoPrato(pratoAtual);
        }

        private void IncluirNovoPrato(Prato prato)
        {
            _interacao.CarregarInterfaceDesisto();

            if (_novoPrato != null)
            {
                _interacao.CarregarInterfaceComplete(prato.Nome, _novoPrato.Nome);

                if (_novoPrato != null)
                {
                    _novoPrato.Id = prato.Id;

                    _novoPratoAssociado.Id = prato.Id - 10;
                    _novoPratoAssociado.PratoAssociado = prato.PratoAssociado;

                    _pratos.Add(_novoPrato);
                    _pratos.Add(_novoPratoAssociado);

                    LimparNovosPratos();
                }
            }
        }
        #endregion
    }
}
